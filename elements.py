import abc
from .misc import intersect, normalSlope, createEdgeFunc
import numpy as np


defaultRayOrigin = np.array([0, 0])
defaultRayDirection = np.array([1, 0])
defaultRayLength = 1e3
veryLargeNum = 1e20


class ElementNotFoundError(Exception):
    def __init__(self, name, message="element '%s' not found",
                 *args, **kwargs):
        # apply name to message
        message = message % name

        # call parent constructor
        super().__init__(message, *args, **kwargs)

        self.name = name


class BaseElement(metaclass=abc.ABCMeta):
    """Represents a source or sink of a function, with a unit.

    This is an abstract representation of components and rays or noise sources.
    """
    # Element type. Represents whether this element
    # behaves like a component,ray, etc.
    ELEMENT_TYPE = None

    @property
    def element_type(self):
        return self.ELEMENT_TYPE


class Component(BaseElement, metaclass=abc.ABCMeta):
    """Represents a layout component.

    Parameters
    ----------
    name : :class:`str`, optional
        The component name. Must be unique.
    refInd : :class:`float`

    Attributes
    ----------
    vertices : :class:`list` of :class:`np.ndarray`
        The vertices of the component. List of pair of coordinates.
    edges : :class:`list`
        List of list in which 1st component is a function of edge and
        the next two components are edge end points.
    """
    ELEMENT_TYPE = "component"
    BASE_NAME = "C"

    def __init__(self, name=None, refInd=1.5, vertices=None):
        super().__init__()
        if name is not None:
            name = str(name)
        # Defaults.
        self.autonamed = False

        self.name = name
        self.refInd = refInd
        self.vertices = vertices

        self.edge_list = []

    @property
    def edges(self):
        for ii in range(len(self.vertices)):
            ii_next = np.mod(ii+1, len(self.vertices))
            edgeFunc = createEdgeFunc(self.vertices[ii],
                                      self.vertices[ii_next])
            self.edge_list += [[edgeFunc,
                                self.vertices[ii],
                                self.vertices[ii_next]]]
        return self.edge_list

    @property
    def label(self):
        """Get component label.

        Returns
        -------
        :class:`str`
            The component label.
        """
        name = self.name

        if name is None:
            # Name not set.
            name = f"{self.__class__.__name__} (no name)"

        return name

    def __repr__(self):
        print('---------- Component ----------')
        print('Name: ', self.name)
        print('Vertices: ', self.vertices)
        print('Refractive Index: ', self.refInd)
        return str(self)

    def __str__(self):
        return self.label

    def __eq__(self, other):
        return self.name == getattr(other, "name", None)

    def __hash__(self):
        """Components uniquely defined by their name"""
        return hash((self.name))

    def encloses(self, point):
        """Whether the component polygon encloses a point"""
        intersectionCounts = 0
        for ii in range(len(self.vertices)):
            ii_next = np.mod(ii+1, len(self.vertices))
            if intersect(u1=self.vertices[ii], u2=self.vertices[ii_next],
                         v1=point, v2=np.array([veryLargeNum, point[1]])):
                intersectionCounts = intersectionCounts + 1
        if np.mod(intersectionCounts, 2) == 0:
            return False
        else:
            return True

    @property
    def drawing(self):
        return np.vstack(self.vertices)


class ComponentNotFoundError(ElementNotFoundError):
    def __init__(self, name, *args, **kwargs):
        super().__init__(name=name, message="component '%s' not found",
                         *args, **kwargs)


class Ray(BaseElement, metaclass=abc.ABCMeta):
    """Represents a layout ray.

    Parameters
    ----------
    name : :class:`str`, optional
        The ray name. Must be unique.
    rank : :class:`int`
       0 for source. Increases with number of surface interfaces met.
    origin : :class:`np.ndarray`
       Length 2 np.ndarray denoting coordinates of start of ray.
    direction : :class:`np.ndarray`
       Length 2 np.ndarray denoting unit vector in the direction of
       ray propagation.
    refInd : :class:`float`

    Attributes
    ----------
    endPoint : :class:`np.ndarray`
       Length 2 np.ndarray denoting coordinates of end of ray.
    """
    ELEMENT_TYPE = "ray"
    BASE_NAME = "R"

    def __init__(self, name=None, rank=0, origin=defaultRayOrigin,
                 direction=defaultRayDirection, refInd=1, info=None):
        super().__init__()
        if name is not None:
            name = str(name)
        # Defaults.
        self.autonamed = False

        self.name = name
        self.rank = rank
        self.origin = origin
        self.direction = direction
        if np.linalg.norm(self.direction) != 1.0:
            self.direction = self.direction/np.linalg.norm(self.direction)
        self.endPoint = None
        self.refInd = refInd
        self.info = info
        self.color = None
        self.arrow = []

    @property
    def label(self):
        """Get ray label.

        Returns
        -------
        :class:`str`
            The ray label.
        """
        name = self.name

        if name is None:
            # Name not set.
            name = f"{self.__class__.__name__} (no name)"

        return name

    def __repr__(self):
        print('---------- Ray ----------')
        print('Name: ', self.name)
        print('Origin: (', self.origin[0], ', ', self.origin[1], ')')
        print('Direction: ', self.direction)
        if self.endPoint is not None:
            print('End Point: (', self.endPoint[0], ', ',
                  self.endPoint[1], ')')
        print('Rank: ', self.rank)
        print('Refractive Index of medium: ', self.refInd)
        if self.info is not None:
            print('Info: ', self.info)
        return str(self)

    def __str__(self):
        return self.label

    def __eq__(self, other):
        return self.name == getattr(other, "name", None)

    def __hash__(self):
        """rays uniquely defined by their name"""
        return hash((self.name))

    @property
    def x_axis(self):
        if self.endPoint is not None:
            return np.array([self.origin[0], self.endPoint[0]])
        else:
            x_end = (self.origin + defaultRayLength*self.direction)[0]
            return np.array([self.origin[0], x_end])

    @property
    def y_axis(self):
        if self.endPoint is not None:
            return np.array([self.origin[1], self.endPoint[1]])
        else:
            y_end = (self.origin + defaultRayLength*self.direction)[1]
            return np.array([self.origin[1], y_end])

    @property
    def angle(self):
        '''Angle from x-axis'''
        if self.direction[0] != 0:
            angle = np.arctan(self.direction[1]/self.direction[0])
        else:
            angle = self.direction[1]*np.inf
        return angle*180/np.pi

    def refract(self, interfaceFunc, nextRefInd):
        interfaceNormalSlope = normalSlope(interfaceFunc)
        normalAng = np.arctan(interfaceNormalSlope)
        normal = np.array([np.cos(normalAng), np.sin(normalAng)])
        if np.dot(normal, self.direction) < 0:
            normal = -normal
        dotp = np.dot(normal, self.direction)
        crossp = np.cross(self.direction, normal)
        sinRefAng = self.refInd*np.linalg.norm(crossp)/nextRefInd
        if np.abs(sinRefAng) < 1:
            cosRefAng = np.sqrt(1-sinRefAng**2)
            refRayDir = (((self.refInd/nextRefInd)
                          * (self.direction - normal*dotp))
                         + normal*cosRefAng)
            return refRayDir
        else:
            return np.array([0.0, 0.0])  # Case of TIR, no refraction
    '''
    def refract(self, interfaceFunc, nextRefInd):
        interfaceNormalSlope = normalSlope(interfaceFunc)
        print(interfaceNormalSlope)
        if np.abs(interfaceNormalSlope) != np.inf:
            if self.direction[0] != 0.0:
                raySlope = self.direction[1]/self.direction[0]
                tanInc = np.abs((interfaceNormalSlope - raySlope)
                                /(1 + interfaceNormalSlope*raySlope))
            else:
                tanInc = np.abs(1/interfaceNormalSlope)
        else:
            if self.direction[1] != 0.0:
                tanInc = np.abs(self.direction[0]/self.direction[1])
                if self.direction[0] != 0.0:
                    raySlope = self.direction[1]/self.direction[0]
                else:
                    raySlope = self.direction[1]*np.inf
            else:
                tanInc = np.inf


        sinRefAngle = (self.refInd*tanInc
                       /(np.sqrt(1 + tanInc**2)*nextRefInd))
        if sinRefAngle < 1.0:
            tanRefAngle = sinRefAngle/np.sqrt(1 - sinRefAngle**2)
            if np.abs(interfaceNormalSlope) != np.inf:
                refRayOpt1 = ((interfaceNormalSlope - tanRefAngle)
                               /(1 + interfaceNormalSlope*tanRefAngle))
                refRayOpt2 = ((interfaceNormalSlope + tanRefAngle)
                               /(1 - interfaceNormalSlope*tanRefAngle))
            else:
                refRayOpt1 = 1/tanRefAngle
                refRayOpt2 = -1/tanRefAngle
            if self.direction[0] != 0.0:
                diffTan1 = (refRayOpt1 - raySlope)/(1 + refRayOpt1*raySlope)
                diffTan2 = (refRayOpt2 - raySlope)/(1 + refRayOpt2*raySlope)
            else:
                diffTan1 = -1/refRayOpt1
                diffTan2 = -1/refRayOpt2
            if diffTan1 > diffTan2:
                refRaySlope = refRayOpt1
            else:
                refRaySlope = refRayOpt2
            sinRefRay = refRaySlope/np.sqrt(1 + refRaySlope**2)
            cosRefRay = np.sqrt(1 - sinRefRay**2)
            refRayDir = np.array([cosRefRay, sinRefRay])
            if np.dot(refRayDir,self.direction)>0:
                return refRayDir
            else:
                return -refRayDir
        else:
            return np.array([0.0, 0.0])  # Case of TIR, no refraction
    '''

    def reflect(self, interfaceFunc):
        interfaceNormalSlope = normalSlope(interfaceFunc)
        normalAng = np.arctan(interfaceNormalSlope)
        normal = np.array([np.cos(normalAng), np.sin(normalAng)])
        if np.dot(normal, self.direction) < 0:
            normal = -normal
        return self.direction - 2*normal*np.dot(normal, self.direction)
    '''
    def reflect(self, interfaceFunc):
        interfaceNormalSlope = normalSlope(interfaceFunc)
        if np.abs(interfaceNormalSlope) != np.inf:
            if self.direction[0] != 0.0:
                raySlope = self.direction[1]/self.direction[0]
                tanInc = np.abs((interfaceNormalSlope - raySlope)
                                /(1 + interfaceNormalSlope*raySlope))
            else:
                tanInc = np.abs(1/interfaceNormalSlope)
        else:
            if self.direction[1] != 0.0:
                tanInc = 0.0
            else:
                tanInc = np.abs(self.direction[0]/self.direction[1])


        refRaySlope = ((interfaceNormalSlope - tanInc)
                       /(1 + interfaceNormalSlope*tanInc))
        sinRefRay = refRaySlope/np.sqrt(1 + refRaySlope**2)
        cosRefRay = np.sqrt(1 - sinRefRay**2)
        refRayDir = np.array([cosRefRay, sinRefRay])

        # Choose the direction of ray
        if np.dot(refRayDir,self.direction)<0:
            return refRayDir
        else:
            return -refRayDir
    '''


class RayNotFoundError(ElementNotFoundError):
    def __init__(self, name, *args, **kwargs):
        super().__init__(name=name, message="ray '%s' not found",
                         *args, **kwargs)
