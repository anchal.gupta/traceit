'''
Author: Anchal Gupta
Date: 09/18/2019
Email: anchal@caltech.edu

---------------------------------traceit------------------------------------->
Traceit is a simple python module to draw ray traces for 2-D visualization
of reflection/transmission of rays through different components.
Currently, this newly born baby has very limited but useful capacity. All
polygonal shaped optics can be inserted and a simple 'zero' inspired
object oriented environment is used.
I declare, that I have taken most of the code style and class structure
directly from Sean Leavey's zero https://git.ligo.org/sean-leavey/zero
All similarities are completely intentional.

For contributions, please adhere to PEP8 code style.
'''

from .misc import intersect, intersectionPoint, distance
from .elements import (Component, Ray, ElementNotFoundError,
                       ComponentNotFoundError, RayNotFoundError)
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection

origin = np.array([0.0, 0.0])


class Layout:
    """Represents an optical layout containing components

    A layout contains components like :class:`components`.
    These are added to the layout via the :meth:`add_component` method.

    Attributes
    ----------
    components : :class:`list` of :class:`.Component`
        The optical layout components.
    rays : :class:`list` of :class:`.Ray`
        The optical layout rays.
    """

    # Disallowed component names.
    RESERVED_NAMES = ["all"]

    def __init__(self, mediumRefInd=1.0):
        # Empty components and rays list.
        self.components = []
        self.rays = []
        self.largeNumber = 1e6    # Can be changed externally.
        self.smallNumber = 1e-6   # Can be changed externally
        self.mediumRefInd = mediumRefInd
        self.arrowHeadSizeFactor = 0.025   # Can be changed externally

    def __getitem__(self, key):
        return self.get_element(key)

    def __contains__(self, key):
        return self.has_element(key)

    def add_component(self, component):
        """Add existing component to layout.

        Parameters
        ----------
        component : :class:`.Component`
            The component to add.

        Raises
        ------
        ValueError
            If component is None, or already present in the layout.
        """
        if component is None:
            raise ValueError("component cannot be None")
        if component.name is None:
            # Assign name.
            self._set_default_name(component)
        if component.name in self:
            raise ValueError(f"element with name"
                             " '{component.name}' already in layout")
        elif component.name in self.RESERVED_NAMES:
            raise ValueError(f"component name '{component.name}' is reserved")
        # Add component to end of list.
        self.components.append(component)

    def add_wedge(self, name=None, refInd=1.5, width=10, height=25.4,
                  wedge_angle=0, corner=origin):
        ex = np.array([1, 0])
        ey = np.array([0, 1])
        vertices = [corner]
        vertices.append(corner + width*ex)
        vertices.append(corner + width*ex + height*ey)
        vertices.append(corner
                        + height*ey
                        - width*np.tan(wedge_angle*np.pi/180.0)*ex)
        wedge = Component(name=name, refInd=refInd, vertices=vertices)
        self.add_component(wedge)

    def add_ray(self, ray):
        """Add existing ray to layout.

        Parameters
        ----------
        component : :class:`.Ray`
            The ray to add.

        Raises
        ------
        ValueError
            If ray is None, or already present in the layout.
        """
        if ray is None:
            raise ValueError("ray cannot be None")
        if ray.name is None:
            # Assign name.
            self._set_default_name(ray)
        if ray.name in self:
            raise ValueError(f"element with name"
                             " '{ray.name}' already in layout")
        elif ray.name in self.RESERVED_NAMES:
            raise ValueError(f"ray name '{ray.name}' is reserved")
        # Add ray to end of list.
        self.rays.append(ray)

    def _set_default_name(self, element):
        """Set a default name unique to this layout for
        the specified element

        Parameters
        ----------
        element : :class:`Component` or :class:`Ray`
            The element to set the name for.
        """
        # base name of element
        base = element.BASE_NAME

        # number to append
        count = 1

        # first attempt
        new_name = f"{base}{count}"

        while new_name in self.element_names:
            # next attempt
            count += 1
            new_name = f"{base}{count}"

        # set new name
        element.name = new_name

        # set flag showing element has been automatically named
        element.autonamed = True

    @property
    def element_names(self):
        """The names of the elements in the circuit.

        Returns
        -------
        :class:`list` of :class:`str`
            The element names.
        """
        return [element.name for element in self.components + self.rays]

    def get_element(self, element_name):
        """Get layout element (component or ray) by name.

        Parameters
        ----------
        element_name : :class:`str`
            The name of the element to fetch.

        Returns
        -------
        :class:`.Component` or :class:`.ray`
            The found component or element.

        Raises
        ------
        :class:`ElementNotFoundError`
            If the element is not found.
        """
        name = element_name.lower()
        try:
            return self.get_component(name)
        except ComponentNotFoundError:
            pass
        try:
            return self.get_ray(name)
        except RayNotFoundError:
            pass
        raise ElementNotFoundError(element_name)

    def has_element(self, element_name):
        """Check if element (component or ray) is present in layout.

        Parameters
        ----------
        element_name : str
            The name of the element to check.

        Returns
        -------
        :class:`bool`
            True if element exists, False otherwise.
        """
        return self.has_component(element_name) or self.has_ray(element_name)

    def get_component(self, component_name):
        """Get layout component by name.

        Parameters
        ----------
        component_name : :class:`str` or :class:`.Component`
            The name of the component to fetch.

        Returns
        -------
        :class:`.Component`
            The component.

        Raises
        ------
        :class:`.ComponentNotFoundError`
            If the component is not found.
        """
        # Get the component name from the object, if appropriate.
        component_name = getattr(component_name, "name", component_name)
        name = component_name.lower()
        for component in self.components:
            if name == component.name.lower():
                return component
        raise ComponentNotFoundError(component_name)

    def replace_component(self, current_component, new_component):
        """Replace layout component with a new one.

        This can be used to replace components.

        Parameters
        ----------
        current_component : :class:`.Component`
            The component to replace.
        new_component : :class:`str` or :class:`.Component`
            The new component.

        Raises
        ------
        :class:`.ComponentNotFoundError`
            If the current component is not in the layout.
        ValueError
            If the new component is already in the layout.
        """
        current_component = self.get_component(current_component)
        if new_component in self.components:
            raise ValueError(f"{new_component} is already in the layout")

        # Do the replacement.
        self.remove_component(current_component)
        self.add_component(new_component)

    def rotate_component(self, cmp_name, rotationCenter=origin, angle=0.0):
        cmp = self.get_component(cmp_name)
        theta = np.radians(angle)
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c, -s), (s, c)))
        for ii, vertex in enumerate(cmp.vertices):
            vertex = np.matmul(R, (vertex - rotationCenter)) + rotationCenter
            cmp.vertices[ii] = vertex

    def has_component(self, component_name):
        """Check if component is present in layout.

        Parameters
        ----------
        component_name : str
            The name of the component to check.

        Returns
        -------
        :class:`bool`
            True if component exists, False otherwise.
        """
        try:
            self.get_component(component_name)
        except ComponentNotFoundError:
            return False
        return True

    def get_ray(self, ray_name):
        """Get layout ray by name.

        Parameters
        ----------
        ray_name : :class:`str` or :class:`.ray`
            The name of the ray to fetch.

        Returns
        -------
        :class:`.ray`
            The ray.

        Raises
        ------
        :class:`RayNotFoundError`
            If the ray is not found.
        """
        # Get the ray name from the object, if appropriate.
        ray_name = getattr(ray_name, "name", ray_name)
        name = ray_name.lower()
        for ray in self.rays:
            if name == ray.name.lower():
                return ray
        raise RayNotFoundError(name)

    def has_ray(self, ray_name):
        """Check if ray is present in layout.

        Parameters
        ----------
        ray_name : str
            The name of the ray to check.

        Returns
        -------
        :class:`bool`
            True if ray exists, False otherwise.
        """
        try:
            self.get_ray(ray_name)
        except RayNotFoundError:
            return False
        return True

    def propagate(self, ray_name, rankLimit=4):
        """Propagate the ray given by ray_name

        Parameters
        ----------
        ray_name : str
            The name of the ray to propagate.
        rankLimit : int
            Stop propagating rays greater or equal to this rank.

        Returns
        -------
        :class:`str`
            Name of the next ray to propagate
        """
        ray = self.get_ray(ray_name)
        '''
        print('---------------------------------------------------')
        print('----Working on Ray ',ray_name,'------------------')
        # '''
        if ray.rank < rankLimit:
            closestIPDistance = np.inf
            intersectionFound = False
            rayFarEndPoint = ray.origin + self.largeNumber*ray.direction
            for cmp in self.components:
                for edge in cmp.edges:
                    if intersect(ray.origin, rayFarEndPoint,
                                 edge[1], edge[2]):
                        ip = intersectionPoint(ray.origin, rayFarEndPoint,
                                               edge[1], edge[2])
                        ipDistance = distance(ray.origin, ip)
                        # Checking for sure if ip is in forward direction.
                        if np.dot((ip-ray.origin), ray.direction) > 0:
                            if ipDistance < closestIPDistance:
                                closestIPDistance = ipDistance
                                ray.endPoint = ip
                                intersectionFound = True
                                interfaceFunc = edge[0]
                                # intersectionEdge = edge
            if intersectionFound:
                '''
                print('Intersection found with edge: ', intersectionEdge)
                print('Intersection Point(IP): ', ray.endPoint)
                print('Origin: ', ray.origin)
                print('Distance to IP: ', closestIPDistance)
                # '''
                nextPoint = ray.endPoint + self.smallNumber*ray.direction
                nextRefInd = self.mediumRefInd
                for cmp in self.components:
                    if cmp.encloses(nextPoint):
                        nextRefInd = cmp.refInd
                transRayDir = ray.refract(interfaceFunc, nextRefInd)

                # Add Transmitted ray if not case of TIR.
                if np.any(transRayDir):
                    transRay = Ray(rank=ray.rank+1, origin=ray.endPoint,
                                   refInd=nextRefInd, direction=transRayDir,
                                   info='Transmitted from '+ray.name)

                    self.add_ray(transRay)
                    self.propagate(transRay.name, rankLimit=rankLimit)
                    reflInfo = 'Reflected from '+ray.name
                else:
                    reflInfo = 'TIR from '+ray.name

                reflRayDir = ray.reflect(interfaceFunc)
                reflRay = Ray(rank=ray.rank+1, origin=ray.endPoint,
                              refInd=ray.refInd, direction=reflRayDir,
                              info=reflInfo)
                self.add_ray(reflRay)
                self.propagate(reflRay.name, rankLimit=rankLimit)

    def draw(self):
        fig = plt.figure(figsize=[16, 12])
        ax = fig.add_subplot(1, 1, 1)
        patches = []
        cmpx_lims = [0, 0]
        cmpy_lims = [0, 0]
        for cmp in self.components:
            cmpCoord = cmp.drawing
            cmpx_lims = [min(cmpx_lims[0], np.min(cmpCoord[:, 0])),
                         max(cmpx_lims[1], np.max(cmpCoord[:, 0]))]
            cmpy_lims = [min(cmpy_lims[0], np.min(cmpCoord[:, 1])),
                         max(cmpy_lims[1], np.max(cmpCoord[:, 1]))]
            patches.append(Polygon(cmpCoord, True))
        p = PatchCollection(patches, alpha=0.4)
        ax.add_collection(p)
        self._drawRays(ax, cmpx_lims, cmpy_lims)
        return fig

    def zoomTo(self, cmp_name, fig=None):
        cmp = self.get_component(cmp_name)
        if fig is None:
            fig = self.draw()
        cmpCoord = cmp.drawing
        cmpx_lims = [np.min(cmpCoord[:, 0]), np.max(cmpCoord[:, 0])]
        cmpy_lims = [np.min(cmpCoord[:, 1]), np.max(cmpCoord[:, 1])]
        self._drawRays(fig.axes[0], cmpx_lims, cmpy_lims, keepCmpLimits=True,
                       drawRays=False)
        return fig

    def zoomAt(self, x_lims, y_lims, fig=None):
        if fig is None:
            fig = self.draw()
        self._drawRays(fig.axes[0], x_lims, y_lims, keepCmpLimits=True,
                       drawRays=False)
        return fig

    def _drawRays(self, ax, cmpx_lims, cmpy_lims, keepCmpLimits=False,
                  drawRays=True):
        cmpSize = [cmpx_lims[1]-cmpx_lims[0], cmpy_lims[1]-cmpy_lims[0]]
        cmpSizeF = np.mean(cmpSize)
        x_lims = [0, 0]
        y_lims = [0, 0]
        refLS = ['-.', '--', ':']
        refLScounter = 0
        for ray in self.rays:
            if drawRays:
                ls = '-'
                if ray.info is not None:
                    if ray.info.find('Transmitted') == -1:
                        ls = refLS[refLScounter]
                        refLScounter = np.mod(refLScounter + 1, 3)
                r, = ax.plot(ray.x_axis, ray.y_axis, label=ray.name, ls=ls)
                ray.color = r.get_color()
            if ray.rank == 0:
                ax.scatter(ray.origin[0], ray.origin[1], marker='*', c='k')
            x_lims = [min(x_lims[0], ray.origin[0]),
                      max(x_lims[1], ray.origin[0])]
            y_lims = [min(y_lims[0], ray.origin[1]),
                      max(y_lims[1], ray.origin[1])]
        x_lims = [min(cmpx_lims[0], x_lims[0]),
                  max(cmpx_lims[1], x_lims[1])]
        y_lims = [min(cmpy_lims[0], y_lims[0]),
                  max(cmpy_lims[1], y_lims[1])]
        y_lims = [y_lims[0]-0.5*cmpSize[1], y_lims[1]+0.5*cmpSize[1]]
        x_lims = [x_lims[0]-0.5*cmpSize[0], x_lims[1]+0.5*cmpSize[0]]
        if keepCmpLimits:
            x_lims = [cmpx_lims[0]-0.1*cmpSize[0], cmpx_lims[1]+0.1*cmpSize[0]]
            y_lims = [cmpy_lims[0]-0.1*cmpSize[1], cmpy_lims[1]+0.1*cmpSize[1]]
        ax.set_ylim(y_lims)
        ax.set_xlim(x_lims)
        # Drawing arrows
        for ray in self.rays:
            if ray.x_axis[0] < x_lims[0]:
                frameStrx = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [x_lims[0],
                                               -self.largeNumber],
                                              [x_lims[0],
                                               self.largeNumber])
            elif ray.x_axis[0] > x_lims[1]:
                frameStrx = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [x_lims[1],
                                               -self.largeNumber],
                                              [x_lims[1],
                                               self.largeNumber])
            else:
                frameStrx = np.array([ray.x_axis[0], ray.y_axis[0]])
            if ray.y_axis[0] < y_lims[0]:
                frameStry = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [-self.largeNumber,
                                               y_lims[0]],
                                              [self.largeNumber,
                                               y_lims[0]])
            elif ray.y_axis[0] > y_lims[1]:
                frameStry = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [-self.largeNumber,
                                               y_lims[1]],
                                              [self.largeNumber,
                                               y_lims[1]])
            else:
                frameStry = np.array([ray.x_axis[0], ray.y_axis[0]])
            if ray.x_axis[1] < x_lims[0]:
                frameEndx = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [x_lims[0],
                                               -self.largeNumber],
                                              [x_lims[0],
                                               self.largeNumber])
            elif ray.x_axis[1] > x_lims[1]:
                frameEndx = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [x_lims[1],
                                               -self.largeNumber],
                                              [x_lims[1],
                                               self.largeNumber])
            else:
                frameEndx = np.array([ray.x_axis[1], ray.y_axis[1]])
            if ray.y_axis[1] < y_lims[0]:
                frameEndy = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [-self.largeNumber,
                                               y_lims[0]],
                                              [self.largeNumber,
                                               y_lims[0]])
            elif ray.y_axis[1] > y_lims[1]:
                frameEndy = intersectionPoint(ray.origin,
                                              [ray.x_axis[1],
                                               ray.y_axis[1]],
                                              [-self.largeNumber,
                                               y_lims[1]],
                                              [self.largeNumber,
                                               y_lims[1]])
            else:
                frameEndy = np.array([ray.x_axis[1], ray.y_axis[1]])
            frameList = [[frameEndx, frameStrx], [frameEndx, frameStry],
                         [frameEndy, frameStrx], [frameEndy, frameStry]]
            minLen = np.inf
            for ii, frame in enumerate(frameList):
                frameLen = distance(frame[0], frame[1])
                if frameLen < minLen:
                    minLen = frameLen
                    minFrameInd = ii
            frameEnd = frameList[minFrameInd][0]
            frameStr = frameList[minFrameInd][1]
            arrow = (frameEnd + 2*frameStr)/3
            rayDrawLen = distance(frameEnd, frameStr)
            arrowd = self.arrowHeadSizeFactor*cmpSizeF*ray.direction
            arrowLen = distance(arrow, arrow+arrowd)
            if 0.3*rayDrawLen < arrowLen:
                arrowd = 0.3 * rayDrawLen * ray.direction
            arrowHead = np.min([cmpSizeF*self.arrowHeadSizeFactor,
                                0.3*rayDrawLen])

            # Remove any older arrows in the same figure.
            # This only works if zoomAt or zoomTo is used with an
            # existing figure object.
            if len(ray.arrow) > 0:
                for rayArr in ray.arrow:
                    if rayArr in ax.artists:
                        try:
                            rayArr.remove()
                        except BaseException:
                            pass
            ray.arrow += [ax.arrow(arrow[0], arrow[1], arrowd[0], arrowd[1],
                                   edgecolor=ray.color, facecolor=ray.color,
                                   head_length=arrowHead, head_width=arrowHead,
                                   length_includes_head=True)]

        ax.grid()
        ax.legend()
