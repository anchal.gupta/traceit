import numpy as np


def intersect(u1, u2, v1, v2):
    """Check if two line segments intersect.

    Parameters
    ----------
    u1 : Length two np.ndarray, end 1 of line segment u
    u2 : Length two np.ndarray, end 2 of line segment u
    v1 : Length two np.ndarray, end 1 of line segment v
    v2 : Length two np.ndarray, end 2 of line segment v

    Returns
    -------
    True or False indicating intersection.
    """
    if lineFunc(u1, u2, v1)*lineFunc(u1, u2, v2) < 0:
        if lineFunc(v1, v2, u1)*lineFunc(v1, v2, u2) < 0:
            return True
    return False


def lineFunc(u1, u2, point):
    a = u2[1] - u1[1]
    b = u1[0] - u2[0]
    c = -(a*u1[0] + b*u1[1])
    if np.abs(a) == np.inf:
        a = 0
        b = 1
        if np.abs(u1[1]) == np.inf:
            c = -u2[1]
        else:
            c = -u1[1]
    elif np.abs(b) == np.inf:
        b = 0
        a = 1
        if np.abs(u1[0]) == np.inf:
            c = -u2[0]
        else:
            c = -u1[0]
    return a*point[0] + b*point[1] + c


def createEdgeFunc(u1, u2):
    def edgeFunc(point):
        return lineFunc(u1, u2, point)
    return edgeFunc


def intersectionPoint(u1, u2, v1, v2):
    x1 = u1[0]
    y1 = u1[1]
    x2 = u2[0]
    y2 = u2[1]
    x3 = v1[0]
    y3 = v1[1]
    x4 = v2[0]
    y4 = v2[1]
    determinant = (x2 - x1)*(y4 - y3) - (x4 - x3)*(y2 - y1)
    x = ((x2*y1 - x1*y2)*(x4-x3) - (x4*y3 - x3*y4)*(x2-x1))/determinant
    y = ((x2*y1 - x1*y2)*(y4-y3) - (x4*y3 - x3*y4)*(y2-y1))/determinant
    return np.array([x, y])


def distance(u1, u2):
    return np.linalg.norm(u1-u2)


def normalSlope(lineFunc):
    c = lineFunc(np.array([0, 0]))
    a = lineFunc(np.array([1, 0])) - c
    b = lineFunc(np.array([0, 1])) - c
    if a != 0.0:
        return b/a
    else:
        return b*np.inf
